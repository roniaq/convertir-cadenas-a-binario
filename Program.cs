﻿using System;


namespace Cadenas
{
   class Program
   {
      static void Main(string[] args)
      { int opcion=0;
         string entrada;
         string salida = "";

         do
         {
            Console.Clear(); //Limpiar pantalla

            Console.WriteLine("Ingrese la cadena a comunicar a la computadora");
            entrada = Console.ReadLine();
            foreach (char caracter in entrada)
            {
               int valor = (int)caracter;
               while (valor / 2 > 0)
               {
                  salida = (valor % 2) + salida;
                  valor = valor / 2;
                  if (valor == 1)
                     salida = (valor % 2) + salida;
               }
               if (salida.Length < 8)
               {
                  int largo = salida.Length;
                  int falta = 8 - largo;
                  for (int i = 1; i <= falta; i++)
                  {
                     salida = '0' + salida;
                  }
               }
               Console.Write(salida + ' ');
               salida = null;
            }

            Console.WriteLine("\n\nMENÚ DE OPCIONES: ");
            Console.WriteLine("\t1. Volve a operar");
            Console.WriteLine("\t2. Salir");
            Console.Write("\n¿Qué opcion elige?:... ");
            try
            {
               opcion = Convert.ToInt32(Console.ReadLine());
            }
            catch(Exception error)
            {
               Console.WriteLine(error);
               opcion = 1;
            }
         } while (opcion==1);
         
      }
   }
}
